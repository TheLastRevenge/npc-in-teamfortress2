
#include "CAI_NPC.h"

ConVar *sv_gravity = NULL;
ConVar *phys_pushscale = NULL;
ConVar *npc_height_adjust = NULL;
ConVar *ai_path_adjust_speed_on_immediate_turns = NULL;
ConVar *ai_path_insert_pause_at_obstruction = NULL;
ConVar *ai_path_insert_pause_at_est_end = NULL;
ConVar *scene_flatturn = NULL;
ConVar *ai_use_clipped_paths = NULL;
ConVar *ai_moveprobe_usetracelist = NULL;
ConVar *ai_use_visibility_cache = NULL;
ConVar *ai_strong_optimizations = NULL;

ConVar *violence_hblood = NULL;
ConVar *violence_ablood = NULL;
ConVar *violence_hgibs = NULL;
ConVar *violence_agibs = NULL;

ConVar *sv_suppress_viewpunch = NULL;
ConVar *ai_navigator_generate_spikes = NULL;
ConVar *ai_navigator_generate_spikes_strength = NULL;

ConVar *ai_no_node_cache = NULL;
ConVar *sv_stepsize = NULL;
ConVar *hl2_episodic = NULL;
ConVar *ai_follow_use_points = NULL;
ConVar *ai_LOS_mode = NULL;
ConVar *ai_follow_use_points_when_moving = NULL;

ConVar *sv_strict_notarget = NULL;

ConVar *ai_shot_bias_min = NULL;
ConVar *ai_shot_bias_max = NULL;
ConVar *ai_shot_bias = NULL;
ConVar *ai_spread_pattern_focus_time = NULL;

ConVar *ai_lead_time = NULL;

CEntity *g_cent;

void Spawn_CommandCallback_Turret(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);

	//vec.Init(6220.0f, 2813.0f, 1090.0f);

	//vec.Init(73.18,-54.81,-60.0);

	//vec.Init(952.65466,61.566082,-58.339985);


	//CEntity *cent = CreateEntityByName("npc_headcrab");
	//CEntity *cent = CreateEntityByName("npc_headcrab_fast");
	//CEntity *cent = CreateEntityByName("npc_headcrab_black");

	//CEntity *cent = CreateEntityByName("npc_fastzombie");
	//CEntity cent = CreateEntityByName("npc_fastzombie_torso");
	//CEntity *cent = CreateEntityByName("npc_zombie_torso");
	//CEntity *cent = CreateEntityByName("npc_zombie");
	//CEntity *cent = CreateEntityByName("npc_poisonzombie");
	
	//CEntity *cent = CreateEntityByName("npc_manhack");
	//CEntity *cent = CreateEntityByName("npc_antlionguard");

	//CEntity *cent = CreateEntityByName("npc_stalker");

	//CEntity *cent = CreateEntityByName("npc_antlion");

	//CEntity *cent = CreateEntityByName("npc_vortigaunt");

	//CEntity *cent = CreateEntityByName("npc_rollermine");
	
	//CEntity *cent = CreateEntityByName("npc_test");
	
	/*CEntity *cent = CreateEntityByName("env_headcrabcanister");
	
	cent->DispatchKeyValue("HeadcrabType", "0");
	cent->DispatchKeyValue("HeadcrabCount", "10");
	
	cent->DispatchKeyValue("SmokeLifetime","60");
	cent->DispatchKeyValue("SkyboxCannisterCount","1");
	cent->DispatchKeyValue("DamageRadius","0");
	cent->DispatchKeyValue("Damage","100");*/

	CEntity *cent = CreateEntityByName("npc_turret_floor");

	CBaseEntity *cbase = cent->BaseEntity();

	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	
	cent->Teleport(&vec, &ang, NULL);

	//cent->AddSpawnFlags(4096);

	cent->Spawn();
	cent->Activate();

	g_cent = cent;

	//hc->Dissolve(NULL, gpGlobals->curtime, false, 0 );

	edict_t *pEdict = servergameents->BaseEntityToEdict(cbase);
	META_CONPRINTF("%p %d %d\n",cbase, cent->entindex_non_network(), engine->IndexOfEdict(pEdict));
}

void Spawn_CommandCallback_Zombie(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_zombie");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_Zombie_Torso(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_zombie_torso");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_PoisonZombie(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_poisonzombie");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_FastZombie(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_fastzombie");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_FastZombie_Torso(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_fastzombie_torso");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_RollerMine(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_rollermine");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_Manhack(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_manhack");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_Vortigaunt(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_vortigaunt");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_Antlion(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_antlion");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_AntlionGuard(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_antlionguard");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_Headcrab(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("npc_headcrab");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->Spawn();
	cent->Activate();
}

void Spawn_CommandCallback_HeadcrabCanister(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("env_headcrabcanister");
	cent->DispatchKeyValue("HeadcrabType", "0");
	cent->DispatchKeyValue("HeadcrabCount", "10");
	cent->DispatchKeyValue("SmokeLifetime","60");
	cent->DispatchKeyValue("SkyboxCannisterCount","1");
	cent->DispatchKeyValue("DamageRadius","0");
	cent->DispatchKeyValue("Damage","100");
	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	cent->Teleport(&vec, &ang,NULL);
	cent->AddSpawnFlags(4096);
	cent->Spawn();
	cent->Activate();
}

void cmd4_CommandCallback(const CCommand &command)
{
	Vector vec(500.0f ,-500.0f, -180.0f);
	QAngle ang(0.0f, 180.0f, 0.0f);
	CEntity *cent = CreateEntityByName("ai_hint");

	CBaseEntity *cbase = cent->BaseEntity();
	CAI_NPC *hc = dynamic_cast<CAI_NPC *>(cent);
	
	cent->Teleport(&vec, &ang,NULL);

	cent->Spawn();
	cent->Activate();

	unsigned int offset;
	datamap_t *pMap = gamehelpers->GetDataMap(cbase);
	if (!pMap)
	{
		return;
	}
	
	typedescription_t *typedesc = gamehelpers->FindInDataMap(pMap, "m_NodeData");
	
	if (typedesc == NULL)
	{
		return;
	}

	offset = typedesc->fieldOffset[TD_OFFSET_NORMAL];
	g_pSM->LogError(myself,"[CENTITY] lookup of prop m_NodeData on entity : %d", offset);
}

void monster_dump_CommandCallback(const CCommand &command)
{
	GetEntityManager()->PrintDump();
}

#define GET_CONVAR(name) \
	name = g_pCVar->FindVar(#name); \
	if(name == NULL) { \
		META_CONPRINTF("[%s] %s - FAIL\n",g_Monster.GetLogTag(), #name); \
		return false; \
	}


bool CommandInitialize()
{
	new ConCommand("spawn_turret",Spawn_CommandCallback_Turret, "");
	new ConCommand("spawn_zombie",Spawn_CommandCallback_Zombie, "");
	new ConCommand("spawn_zombie_torso",Spawn_CommandCallback_Zombie_Torso, "");
	new ConCommand("spawn_poisonzombie",Spawn_CommandCallback_PoisonZombie, "");
	new ConCommand("spawn_fastzombie",Spawn_CommandCallback_FastZombie, "");
	new ConCommand("spawn_fastzombie_torso",Spawn_CommandCallback_FastZombie_Torso, "");
	new ConCommand("spawn_rollermine",Spawn_CommandCallback_RollerMine, "");
	new ConCommand("spawn_manhack",Spawn_CommandCallback_Manhack, "");
	new ConCommand("spawn_vortigaunt",Spawn_CommandCallback_Vortigaunt, "");
	new ConCommand("spawn_antlion",Spawn_CommandCallback_Antlion, "");
	new ConCommand("spawn_antlionguard",Spawn_CommandCallback_AntlionGuard, "");
	new ConCommand("spawn_headcrab",Spawn_CommandCallback_Headcrab, "");
	new ConCommand("spawn_headcrabcanister",Spawn_CommandCallback_HeadcrabCanister, "");
	new ConCommand("e4",cmd4_CommandCallback, "", 0);
	new ConCommand("pp",monster_dump_CommandCallback, "", 0);
	new ConCommand("monster_dump",monster_dump_CommandCallback, "", 0);

	GET_CONVAR(sv_gravity);
	GET_CONVAR(phys_pushscale);
	GET_CONVAR(npc_height_adjust);

	GET_CONVAR(ai_path_adjust_speed_on_immediate_turns);
	GET_CONVAR(ai_path_insert_pause_at_obstruction);
	GET_CONVAR(ai_path_insert_pause_at_est_end);
	GET_CONVAR(ai_use_clipped_paths);
	GET_CONVAR(ai_moveprobe_usetracelist);
	GET_CONVAR(scene_flatturn);

	GET_CONVAR(violence_hblood);
	GET_CONVAR(violence_ablood);
	GET_CONVAR(violence_hgibs);
	GET_CONVAR(violence_agibs);

	GET_CONVAR(sv_suppress_viewpunch);
	GET_CONVAR(ai_use_visibility_cache);
	GET_CONVAR(ai_strong_optimizations);
	GET_CONVAR(ai_navigator_generate_spikes);
	GET_CONVAR(ai_navigator_generate_spikes_strength);
	GET_CONVAR(ai_no_node_cache);

	GET_CONVAR(sv_stepsize);
	GET_CONVAR(hl2_episodic);
	GET_CONVAR(ai_follow_use_points);
	GET_CONVAR(ai_LOS_mode);
	GET_CONVAR(ai_follow_use_points_when_moving);
	
	GET_CONVAR(sv_strict_notarget);

	GET_CONVAR(ai_shot_bias_min);
	GET_CONVAR(ai_shot_bias_max);
	GET_CONVAR(ai_shot_bias);
	GET_CONVAR(ai_spread_pattern_focus_time);

	GET_CONVAR(ai_lead_time);

	return true;
}
